console.log("hello world!");

// [Section] Objects
/**
 * - An object is a data type that is used to represent a real world objects.
 * - It is a collection of related data and/or functionalities/method.
 * - Information stored in objects are represented in a "key:value" pair
 * - Key is also mostly referred to as "property" of an object.
 * - Different data type may be stored in an object's property creating data structures.
 * Syntax:
 *      let/const objectName = {
 *          keyA: valueA,
 *          keyB: valueB
 *      }
 *      - This creates/declares an object and also initializes/assign it's properties upon creation.
 *      - A cellphone is an example of real world object.
 *      - It has its own properties such as name, color, weight, unit model and a lot of other properties.
 */

const cellphone = {
  name: "Nokia 3210",
  manufactureDate: 1999,
};

console.log("Result from creating using literal notation: ");
console.log(cellphone);

// Creating objects using constructor function
/***
 * - Creates a reusable function to create several objects that have the same data structure.
 * - This is usefull for creating multiple instances/copies of an object
 * - An instance is a concreate occurance of any object which emphasize distinct/unique identity of it
 * Syntax:
 *      function objectName(valueA, valueB){
 *          this.keyA = valueA,
 *          this.keyB = valueB
 *       }
 *
 */
/**
 * This is an constructor function
 * this keyword allows us to assign a new object's properties by associating them with values
 */

function Laptop(name, manufacturedDate, ram) {
  this.laptopName = name;
  this.laptopManufacturedDate = manufacturedDate;
  this.laptopRam = ram;
}

/** Instatiation
 *  The "new" operator creates an instances of an object.
 *  Objects literals(let object = {}) and instances (let objectName = new functionName(arguments)) are distinct/unique objects
 */

const laptop = new Laptop("Lenovo", 2008, "2gb");
console.log("Result from creating objects using object Constructor");
console.log(laptop);

const myLaptop = new Laptop("Mac Book Air", 2020);
console.log("Result from creating objects using object Constructor");
console.log(myLaptop);

const oldLaptop = Laptop("Protal 32CC", 1980, "500 mb");

console.log("Result from creating objects without new keyword: ");
console.log(oldLaptop);

/**
 * Mini-activity
 *  You will create a constructor function that will let us instatiate a new object, menu, property: menuName, menuPrice
 */

function Menu(name, price) {
  this.menuName = name;
  this.menuPrice = price;
}

const mealOne = new Menu("Breakfast", 299);
console.log(mealOne);

// creating empty objects
const computer = {};
const myComputer = new Object();
console.log(computer);
console.log(myComputer);

// Accesing objects inside an array;

const array = [laptop, myLaptop];
console.log(array);
console.log(array[0]["laptopName"]);
// dot notation
console.log(array[0].laptopName);

console.log(laptop);
console.log(laptop.laptopName);

// [Section] Initializing/adding/deleting/reassigning Object properties.
/**
 *  - like any other variable in JavaScript, objects have their properties initialized/ added after the object ws created/declared.
 */

let Car = {};
console.log(Car);
// Initializing/adding object properties using dot notation
Car.name = "Honda Civic";
console.log("Adding object property in Car using dot notation: ");
console.log(Car);
// Initializing/adding object properties using bracket notation
Car["manufacturedDate"] = 2019;
console.log("Adding object property in Car using bracket notation: ");
console.log(Car);

// deleting object properties
// deleting using bracket notation
delete Car["name"];
console.log(Car);
// deleting using bracket notation
delete Car.manufacturedDate;
console.log(Car);

// reassigning object properties
// reassign object - dot notation
Car.name = "Dodge Charger R/T";
console.log(Car);
// reassign object - bracket notation
Car["name"] = "Monster Jeep";
console.log(Car);

// [Section] Object Methods
/**
 * - A method is a function which is a property of an object.
 * - They are also functions and one of the key differences the have is that methods are functions related to a specific object
 */

const Person = {
  name: "John",
  talk: function () {
    console.log(`Hello my nme is ${this.name}`);
  },
};
console.log(Person);
Person.talk();

// add method to objects
Person.walk = function () {
  console.log(`${this.name} walked 25 steps forward.`);
};
Person.walk();

// methods are useful for creating reusable functions that perform tasks related to objects
const Friends = {
  firstName: "Joe",
  lastName: "Smith",
  address: {
    city: "Austin",
    country: "Texas",
  },
  phoneNumbers: [["09630741448"], ["0926194123"]],
  emails: ["joe@gmail.com", "joesmith@email.xyz"],
  introduce: function () {
    console.log(
      `Hello my name is ${this.firstName} ${this.lastName}. I live in ${this.address.city} ${this.address.country}. My emails are ${this.emails[0]} and ${this.emails[1]}. My phone numbers are ${this.phoneNumbers[0][0]} and ${this.phoneNumbers[1][0]}`
    );
  },
};
Friends.introduce();

function Pokemon(name, level) {
  // Properties Pokemon
  this.pokemonName = name;
  this.pokemonLevel = level;
  this.pokemonHealth = 2 * level;
  this.pokemonAttack = level;

  // methods
  // We re going to add a method named tackle
  this.tackle = function (targetPokemon) {
    console.log(`${this.pokemonName} tackles ${targetPokemon.pokemonName}`);
    console.log(
      `${targetPokemon.pokemonHealth} is now reduced to ${
        targetPokemon.pokemonHealth - this.pokemonAttack
      }`
    );
  };

  this.fainted = function () {
    console.log(`${this.pokemonName} fainted!`);
  };
}
const pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu);
const gyarados = new Pokemon("Gyarados", 20);
console.log(gyarados);
pikachu.tackle(gyarados);
pikachu.tackle(gyarados);
